namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
	public class CustomerPreference : BaseEntity
	{
		public virtual Customer Customer { get; set; }
		public virtual Preference Preference { get; set; }
	}
}