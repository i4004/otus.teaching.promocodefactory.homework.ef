using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class EfRepository<T>
		 : IRepository<T>
		 where T : BaseEntity
	{
		private readonly DataContext _dataContext;

		public EfRepository(DataContext dataContext) => _dataContext = dataContext;

		public async Task CreateAsync(T entity)
		{
			await _dataContext.Set<T>().AddAsync(entity);
			await _dataContext.SaveChangesAsync();
		}

		public async Task<IEnumerable<T>> GetAllAsync() => await _dataContext.Set<T>().ToListAsync();

		public async Task<T> GetByIdAsync(Guid id) => await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

		public async Task UpdateAsync(T entity)
		{
			_dataContext.Set<T>().Update(entity);

			await _dataContext.SaveChangesAsync();
		}

		public async Task DeleteAsync(T entity)
		{
			_dataContext.Remove(entity);

			await _dataContext.SaveChangesAsync();
		}
	}
}