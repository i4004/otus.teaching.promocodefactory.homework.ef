using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
	public class DataContext : DbContext
	{
		public DataContext(DbContextOptions<DataContext> options) : base(options)
		{
		}

		public DbSet<Employee> Employees { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Preference> Preferences { get; set; }
		public DbSet<Customer> Customers { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Employee>(x =>
			{
				x.HasKey(x => x.Id);

				x.Property(x => x.FirstName).HasMaxLength(256);
				x.Property(x => x.LastName).HasMaxLength(256);
				x.Property(x => x.Email).HasMaxLength(256);

				x.HasOne<Role>(x => x.Role);
			});

			modelBuilder.Entity<Role>(x =>
			{
				x.HasKey(x => x.Id);

				x.Property(x => x.Name).HasMaxLength(256);
				x.Property(x => x.Description).HasMaxLength(512);
			});

			modelBuilder.Entity<Customer>(x =>
			{
				x.HasKey(x => x.Id);

				x.Property(x => x.FirstName).HasMaxLength(256);
				x.Property(x => x.LastName).HasMaxLength(256);
				x.Property(x => x.LastName).HasMaxLength(512);
				x.Property(x => x.Email).HasMaxLength(256);

				x.HasMany(x => x.PromoCodes);
			});

			modelBuilder.Entity<CustomerPreference>(x =>
				{
					x.HasKey(x => x.Id);

					x.HasOne(x => x.Customer);
					x.HasOne(x => x.Preference);
				});

			modelBuilder.Entity<PromoCode>(p =>
			{
				p.Property(x => x.Code).HasMaxLength(256);
				p.Property(x => x.PartnerName).HasMaxLength(256);
				p.Property(x => x.ServiceInfo).HasMaxLength(512);
			});

			modelBuilder.Entity<Preference>(x =>
			{
				x.HasKey(x => x.Id);

				x.Property(x => x.Name).HasMaxLength(256);
			});
		}
	}
}