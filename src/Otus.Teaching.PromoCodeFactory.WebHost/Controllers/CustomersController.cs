﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
	/// <summary>
	/// Клиенты
	/// </summary>
	[ApiController]
	[Route("api/v1/[controller]")]
	public class CustomersController
		: ControllerBase
	{
		private readonly EfRepository<Customer> _customerRepository;
		private readonly EfRepository<Preference> _preferenceRepository;
		private readonly EfRepository<CustomerPreference> _customerPreferenceRepository;
		private readonly EfRepository<PromoCode> _promoCodeRepository;

		public CustomersController(EfRepository<Customer> customerRepository, EfRepository<Preference> preferenceRepository, EfRepository<CustomerPreference> customerPreferenceRepository, EfRepository<PromoCode> promoCodeRepository)
		{
			_customerRepository = customerRepository;
			_preferenceRepository = preferenceRepository;
			_customerPreferenceRepository = customerPreferenceRepository;
			_promoCodeRepository = promoCodeRepository;
		}

		/// <summary>
		/// Get all customers short info.
		/// </summary>
		/// <returns>List of customers short info.</returns>
		[HttpGet]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CustomerShortResponse))]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<ActionResult<ICollection<CustomerShortResponse>>> GetCustomersAsync()
		{
			var customers = await _customerRepository.GetAllAsync();

			var customersShortResponseList = customers.Select(x => new CustomerShortResponse
			{
				Id = x.Id,
				FirstName = x.FirstName,
				LastName = x.LastName,
				Email = x.Email
			}).ToList();

			return customersShortResponseList;
		}

		/// <summary>
		/// Get customer by id.
		/// </summary>
		/// <returns>Customer info by id.</returns>
		[HttpGet("{id}")]
		public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
		{
			var data = await _customerRepository.GetByIdAsync(id);

			if (data == null)
				return NotFound();

			var response = new CustomerResponse()
			{
				Id = data.Id,
				FirstName = data.FirstName,
				LastName = data.LastName,
				Email = data.Email,
				Preferences = new List<PreferenceResponse>()
			};

			response.Preferences = data.CustomerPreferences.Select(x => new PreferenceResponse()
			{
				Id = x.Preference.Id,
				Name = x.Preference.Name
			}).ToList();

			return Ok(response);
		}

		/// <summary>
		/// Create new customer.
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		[ProducesResponseType(StatusCodes.Status201Created)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			var customer = new Customer()
			{
				Id = Guid.NewGuid(),
				FirstName = request.FirstName,
				LastName = request.LastName,
				Email = request.Email,
				CustomerPreferences = new List<CustomerPreference>()
			};

			var preferences = await _preferenceRepository.GetAllAsync();

			var customerPreferences = preferences.Where(x => request.PreferenceIds.Contains(x.Id)).ToList();

			foreach (var customerPreference in customerPreferences)
			{
				customer.CustomerPreferences.Add(new CustomerPreference()
				{
					Customer = customer,
					Preference = customerPreference
				});
			}

			await _customerRepository.CreateAsync(customer);

			var response = new CustomerShortResponse()
			{
				Id = customer.Id,
				FirstName = customer.FirstName,
				LastName = customer.LastName,
				Email = request.Email
			};

			return Ok(response);
		}

		/// <summary>
		/// Update a customer.
		/// </summary>
		/// <returns>Updated employee.</returns>
		[HttpPut("{id}")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			var preferences = await _preferenceRepository.GetAllAsync();
			var customerPreferences = preferences.Where(x => request.PreferenceIds.Contains(x.Id)).ToList();

			var customer = await _customerRepository.GetByIdAsync(id);
			if (customer == null)
			{
				var customerNew = new Customer()
				{
					Id = id,
					FirstName = request.FirstName,
					LastName = request.LastName,
					Email = request.Email,
					CustomerPreferences = new List<CustomerPreference>()
				};

				foreach (var customerPreference in customerPreferences)
				{
					customerNew.CustomerPreferences.Add(new CustomerPreference()
					{
						Customer = customerNew,
						Preference = customerPreference
					});
				}

				await _customerRepository.CreateAsync(customerNew);

				return StatusCode(201);
			}

			customer.Id = id;
			customer.FirstName = request.FirstName;
			customer.LastName = request.LastName;
			customer.Email = request.Email;
			customer.CustomerPreferences.Clear();
			foreach (var customerPreference in customerPreferences)
			{
				customer.CustomerPreferences.Add(new CustomerPreference()
				{
					Customer = customer,
					Preference = customerPreference
				});
			}

			await _customerRepository.UpdateAsync(customer);

			return StatusCode(200);

		}

		/// <summary>
		/// Delete a customer.
		/// </summary>
		/// <returns>No content.</returns>
		[HttpDelete]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> DeleteCustomer(Guid id)
		{
			var item = await _customerRepository.GetByIdAsync(id);

			if (item == null)
				return BadRequest();

			await _customerRepository.DeleteAsync(item);

			return NoContent();
		}
	}
}